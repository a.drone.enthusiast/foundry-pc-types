declare namespace FormApplication {
	interface Options extends Application.Options {
		/**
		 * Whether the application form is editable - if true, it's fields will
		 * be unlocked and the form can be submitted. If false, all form fields
		 * will be disabled and the form cannot be submitted. Default is true.
		 */
		editable?: boolean;
		/**
		 * Whether to automatically close the application when it's contained
		 * form is submitted. Default is true.
		 */
		closeOnSubmit?: boolean;
		/**
		 * Whether to automatically submit the contained HTML form when the
		 * application window is manually closed. Default is false.
		 */
		submitOnClose?: boolean;
		/**
		 * Whether to automatically submit the contained HTML form when an input
		 * or select element is changed. Default is false.
		 */
		submitOnChange?: boolean;
	}
}

/**
 * An abstract pattern for defining an Application responsible for updating some object using an HTML form
 *
 * A few critical assumptions:
 * 1) This application is used to only edit one object at a time
 * 2) The template used contains one (and only one) HTML form as it's outer-most element
 * 3) This abstract layer has no knowledge of what is being updated, so the implementation must define _updateObject
 *
 * @extends {Application}
 * @interface
 *
 * @param object {*}                    Some object or entity which is the target to be updated.
 * @param [options] {Object}            Additional options which modify the rendering of the sheet.
 *
 * @template D	The schema type of the data used to render the inner template.
 * @template OB	The type of the object being edited by the form.
 */
declare abstract class FormApplication<OB = {}> extends Application {
	options: FormApplication.Options;

	/** The object target which we are using this form to modify */
	object: OB;

	/** A convenience reference to the form HTLMElement */
	form: JQuery | HTMLElement;

	/**
	 * Keep track of any FilePicker instances which are associated with this form
	 * The values of this Array are inner-objects with references to the FilePicker instances and other metadata
	 */
	filepickers: FilePicker[];

	/**
	 * Keep track of any mce editors which may be active as part of this form
	 * The values of this object are inner-objects with references to the MCE editor and other metadata
	 */
	editors: Record<string, any>;

	constructor(object?: OB, options?: FormApplication.Options);

	/**
	 * Assign the default options which are supported by the entity edit sheet.
	 * In addition to the default options object supported by the parent Application class, the Form Application
	 * supports the following additional keys and values:
	 *
	 * @return options					The default options for this FormApplication class, see Application
	 * @return options.closeOnSubmit	Whether to automatically close the application when it's contained
	 *									form is submitted. Default is true.
	 * @return options.submitOnChange	Whether to automatically submit the contained HTML form when an input
	 *									or select element is changed. Default is false.
	 * @return options.submitOnClose	Whether to automatically submit the contained HTML form when the
	 *									application window is manually closed. Default is false.
	 * @return options.editable			Whether the application form is editable - if true, it's fields will
	 *									be unlocked and the form can be submitted. If false, all form fields
	 *									will be disabled and the form cannot be submitted. Default is true.
	 */
	static get defaultOptions(): FormApplication.Options;

	/**
	 * Is the Form Application currently editable?
	 */
	get isEditable(): boolean;

	/* -------------------------------------------- */
	/*  Event Listeners and Handlers                */
	/* -------------------------------------------- */

	/**
	 * Activate the default set of listeners for the Entity sheet
	 * These listeners handle basic stuff like form submission or updating images
	 *
	 * @param html	The rendered template ready to have listeners attached
	 */
	protected activateListeners(html: JQuery | HTMLElement): void;

	/**
	 * If the form is not editable, disable its input fields
	 */
	protected _disableFields(form: JQuery | HTMLElement): void;

	/**
	 * Handle the change of a color picker input which enters it's chosen value into a related input field
	 */
	protected _onColorPickerChange(event: Event | JQuery.Event): void;

	/**
	 * Handle standard form submission steps
	 * @param event			The submit event which triggered this handler
	 * @param updateData	Additional specific data keys/values which override or extend the contents of
	 *						the parsed form. This can be used to update other flags or data fields at the
	 *						same time as processing a form submission to avoid multiple database operations.
	 * @param preventClose	Override the standard behavior of whether to close the form on submit
	 * @param preventRender	Prevent the application from re-rendering as a result of form submission
	 * @returns				A promise which resolves to the validated update data
	 */
	protected _onSubmit(
		event: Event | JQuery.Event,
		{
			updateData,
			preventClose,
			preventRender,
		}?: {
			updateData?: object;
			preventClose?: boolean;
			preventRender?: boolean;
		}
	): Promise<any>;

	/**
	 * Get an object of update data used to update the form's target object
	 * @param  updateData	Additional data that should be merged with the form data
	 * @return				The prepared update data
	 */
	protected _getSubmitData(updateData?: object): any;

	/**
	 * Handle changes to an input element, submitting the form if options.submitOnChange is true.
	 * Do not preventDefault in this handler as other interactions on the form may also be occurring.
	 * @param event	The initial change event
	 */
	protected _onChangeInput(event: Event | JQuery.Event): void;

	/**
	 * Handle the change of a color picker input which enters it's chosen value into a related input field
	 */
	protected _onChangeColorPicker(event: Event | JQuery.Event): void;

	/**
	 * Handle changes to a range type input by propagating those changes to the sibling range-value element
	 * @param event	The initial change event
	 */
	protected _onChangeRange(event: Event | JQuery.Event): void;

	/**
	 * This method is called upon form submission after form data is validated
	 * @param event		The initial triggering submission event
	 * @param formData	The object of validated form data with which to update the object
	 * @returns			A Promise which resolves once the update operation has completed
	 */
	protected _updateObject(
		event: Event | JQuery.Event,
		formData: object
	): Promise<any>;

	/* -------------------------------------------- */
	/*  TinyMCE Editor
	/* -------------------------------------------- */

	/**
	 * Activate a named TinyMCE text editor
	 * @param name				The named data field which the editor modifies.
	 * @param options			TinyMCE initialization options passed to TextEditor.create
	 * @param initialContent	Initial text content for the editor area.
	 */
	activateEditor(name: string, options: object, initialContent: string): void;

	/**
	 * Handle saving the content of a specific editor by name
	 * @param name		The named editor to save
	 * @param remove	Remove the editor after saving its content
	 */
	saveEditor(name: string, { remove }?: { remove?: boolean }): Promise<void>;

	/**
	 * Activate a TinyMCE editor instance present within the form
	 */
	protected _activateEditor(div: JQuery | HTMLElement): void;

	/* -------------------------------------------- */
	/*  FilePicker UI
	/* -------------------------------------------- */

	/**
	 * Activate a FilePicker instance present within the form
	 */
	protected _activateFilePicker(button: JQuery | HTMLElement): void;

	/* -------------------------------------------- */
	/*  Methods                                     */
	/* -------------------------------------------- */

	/**
	 * Extend the logic applied when the application is closed to destroy any remaining MCE instances
	 * This function returns a Promise which resolves once the window closing animation concludes
	 */
	close(options?: object): Promise<void>;

	/**
	 * Submit the contents of a Form Application, processing its content as defined by the Application
	 * @param updateData	Additional data updates to submit in addition to those parsed from the form
	 * @returns				Return a self-reference for convenient method chaining
	 */
	submit(options?: object): Promise<FormApplication>;
}
